import importlib, sys, copy
from matplotlib import pyplot as plot
import numpy

algo    = sys.argv[1]
users   = int(sys.argv[2])
rounds  = int(sys.argv[3])

if __name__ == "__main__":
    module  = importlib.import_module(algo)

    init    = module.init
    update  = module.update
    view    = module.view

    users   = init(users)
    history   = []

    last_users = None
    for i in range(rounds):
        if len(sys.argv) >= 6 and sys.argv[4] == "sigmoid":
            round = i
            maximum_user_count = int(sys.argv[5])
            midpoint = maximum_user_count/2
        else:
            round = 0
            midpoint = 0
            maximum_user_count = 0

        users = update(users, history = history)
        if type(users) == tuple:
            last_users = users[1]
            users = users[0]

        history.append(copy.copy(users))


    view(users, history)
