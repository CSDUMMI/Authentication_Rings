# Authentication Rings
This implements and gives analysis on the success on random
input to an algorithm, that ought to determine whether a given
user is "authentic" or not.

It does this, by assigning each user two values between [0,1]:
The authentication score (`auth`) and the correctness score (`corr`).
Besides these, each user can state about another user, that
they are "authentic".
So each user also has a Set of other users, supporting them as being "authentic"
`supporters`.

`auth` and `corr` are now computed like this for each user depending on their
own `suppoerters` and recomputed on every change to their own `supporters` and
those they are `supporting`, both sets of users, one denoting those whom a user
supports as "authentic" and the other of those supporting the user themselves as
"authentic" respectively.

```
auth = max([supporter.corr for support in supporters])
corr = len([supporting_user.auth > thresh for supporting_user in supporting])/
len(supporting)
```
where thresh can be a value between [0, 1] determining the percentage of
uncertainty about a user determined as authentic to be left.
In the example code in `test_Authrings.py` the `tresh = 0.95`, leaving a 5%
uncertainty about the authenticity of any user determined "authentic".
