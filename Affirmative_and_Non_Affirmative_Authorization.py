# Similar to test_Authrings.py,
# but instead of just the positive authentic propabilites,
# in this model each user also has a negative probability,
# that they are not authentic.
# To determine wheter a user is authentic or not,
# the probabilites is_auth - is_not_auth >= 0.95.
import random, sys, math

class User(object):
    def __init__(self, id):
        self.id                 = id
        self.is_auth            = 0
        self.is_not_auth        = 0
        self.authenticity       = random.random() < 0.5
        self.correctness        = 0
        self.true_correctness   = random.triangular(0, 1, 0.75)
        self.supporting_users   = set()
        self.opposing_users     = set()
        self.is_init            = False
        self.init_rounds        = 0
        self.frame              = set() # Frame of the users, considered for authentication by this user
        self.frame_round        = 0


        if not self.is_init:
            self.is_evil        = random.random() < 0.05
            if self.is_evil:
                self.target         = id - random.randint(0, id -1)
            else:
                self.target         = None
        else:
            self.is_evil        = False
            self.target         = 0

    def authentic(self):
        return (self.is_auth - self.is_not_auth) >= 0.95

    def unauthentic(self):
        return (self.is_auth - self.is_not_auth) <= -0.95
    def __str__(self):
        return f"<User: {self.id} {self.authenticity} {self.is_auth - self.is_not_auth} {self.true_correctness} {self.correctness}>"

def update_single_user(user, users, hide_output = False):
    if user.is_init:
        user_testing = random.sample([u for u in user.frame], k = 1)[0]
        if user.frame_round > 2 * len(user.frame):
            user.frame  = random.sample([u for u in users if u != user], k = len(user.frame))
    else:
        user_testing = random.sample([ u for u in users if u != user and not u.is_init], 1)[0]

    be_opposing     = False
    be_supporter    = False

    if user.is_evil and user.correctness >= 0.95:
        users[user.target].opposing_users.add(user)

    if user_testing.authenticity:
        be_supporter    = random.random() < user.true_correctness
        if not be_supporter:
            be_opposing     = random.random() > user.true_correctness
    else:
        be_supporter    = random.random() > user.true_correctness
        if not be_supporter:
            be_opposing     = random.random() < user.true_correctness

    if be_supporter and user.target != user_testing.id:
        if user in user_testing.opposing_users:
            user_testing.opposing_users

        user_testing.supporting_users.add(user)
        if not hide_output:
            print(f"{str(user)} supports {str(user_testing)} with {user.correctness * 100}% correctness")
    elif be_opposing or user.target == user_testing.id:
        if user in user_testing.supporting_users:
            user_testing.supporting_users.remove(user)

        user_testing.opposing_users.add(user)
        if not hide_output:
            print(f"{str(user)} opposes {str(user_testing)} with {user.correctness * 100}% correctness")


def update(users, **kwargs):
    # Support or oppose one random other user.
    for user in users:
        # IRL: Nobody would be very active, when they don't have a higher correctness
        if user.correctness < 0.2:
            if random.random() < 0.8:
                continue

        if not user.is_init or (True not in [u.correctness >= 0.95 for u in users if not u.is_init]):
            update_single_user(user, users, **kwargs)

        if user.true_correctness >= 0.95 or random.random() < 0.2:
            init_user   = [u for u in users if u.is_init][0]
            for i in range(len(init_user.frame)):
                update_single_user(user, init_user.frame, **kwargs)



    # Update is_auth and is_not_auth probability.
    for user in [u for u in users if not u.is_init]:
        user.is_auth        = max([u.correctness for u in [supporter for supporter in user.supporting_users if supporter.authentic()]], default = 0)
        user.is_not_auth    = max([u.correctness for u in [opposer for opposer in user.opposing_users if opposer.authentic()]], default = 0)


    # Update correctness value.
    for user in [u for u in users if not u.is_init]:
        users_opposing      = [u for u in users if user in u.opposing_users]
        users_supporting    = [u for u in users if user in u.supporting_users]

        if len(users_opposing) + len(users_supporting) < math.sqrt(len(users)):
            user.correctness    = 0
        else:
            recognized_unauths  = [u for u in users_opposing if u.unauthentic]
            recognized_auths    = [u for u in users_supporting if u.authentic()]

            user.correctness    = len(recognized_auths) + len(recognized_unauths)
            user.correctness    = user.correctness / (len(users_opposing) + len(users_supporting))

    return users

def init(user_count):
    users                           = [User(i) for i in range(user_count - 1)]
    initial_user                    = User(user_count)
    initial_user.is_init            = True
    initial_user.is_auth            = 1
    initial_user.is_not_auth        = 0
    initial_user.correctness        = 1
    initial_user.true_correctness   = 1
    initial_user.authenticity       = True
    initial_user.is_evil            = False
    initial_user.target             = None
    initial_user.frame              = random.sample(users, k = int(len(users)/4))
    users.append(initial_user)

    for user in users:
        if user.authenticity:
            user.true_correctness   = 0.96
            break

    return users

def view(users):
    authentic           = [u for u in users if u.authenticity]
    unauthentic         = [u for u in users if not u.authenticity]
    recognized_auths    = [u for u in users if u.authenticity and u.authentic()]
    recognized_unauths  = [u for u in users if not u.authenticity and not u.authentic()]

    eligable_voters             = [u for u in users if u.authentic() ]
    authentic_eligable_voters   = [u for u in eligable_voters if u.authenticity]
    unauthentic_eligable_voters = [u for u in eligable_voters if not u.authenticity]

    return f"""# of authentic users: {len(authentic)}
# of unauthentic users: {len(unauthentic)}
Rate of authentic users: {len(authentic)/len(users) * 100}%
Rate of unauthentic users: {len(unauthentic)/len(users) * 100}%
Rate of recognized users: {(len(recognized_auths) + len(recognized_unauths)) / len(users) * 100}%
Rate of recognized authentic users: {len(recognized_auths)/len(authentic) * 100}%
Rate of recognized unauthentic users: {len(recognized_unauths)/len(unauthentic) * 100}%
Division of eligable voters:
{len(authentic_eligable_voters)/len(eligable_voters) * 100}% of the voters are authentic
{len(unauthentic_eligable_voters)/len(eligable_voters) * 100}% of the voters are not authentic
Number of users with correctness of >= 95%:
{len([u for u in users if u.correctness >= 0.95])}
Users with correctness of >= 95%:
{[str(u) for u in users if u.correctness >= 0.95]}
Number of users with true correctness of >= 95%:
{len([u for u in users if u.true_correctness >= 0.95])}
Users with true correctness of >= 95%:
{[str(u) for u in users if u.true_correctness >= 0.95]}
Authentic users without support of an init user:
{[str(u) for u in users if u.authentic() and all({ not supporter.is_init for supporter in u.supporting_users })]}
    """
