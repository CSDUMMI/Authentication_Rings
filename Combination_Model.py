import random, sys, math, threading, os, datetime
from matplotlib import pyplot as plt

class Queue(object):
    def __init__(self, size : int):
        self.elems = [None for i in range(size)]
        self.head = 0
        self.max = size

    def push(self, obj):
        if self.head >= self.max:
            self.head = 0
        head = self.elems[self.head]
        self.elems[self.head] = obj
        self.head += 1
        return head

    def find(self, obj):
        for i in self.elems:
            if obj == i:
                return i

        return False
    def __contains__(self, value):
        return value in self.elems


class User(object):
    def __init__(self, id, is_init = True):
        self.id             = id
        self.is_auth        = 1 if is_init else 0
        self.is_not_auth    = 0
        self.is_init        = is_init
        self.checked        = Queue(20)

        self.true_correctness = .95 if is_init else random.random()
        self.true_authenticity = random.random() < 0.5 if not is_init else True

    def update(self, users):
        if not self.is_init:
            supporters = [u.correctness(users) for u in users if self in u.checked and u.checked.find(self).result]
            opponents = [u.correctness(users) for u in users if self in u.checked and not u.checked.find(self).result]
            self.is_auth = max(supporters, default = 0)
            self.is_not_auth = max(opponents, default = 0)

    def authentic(self):
        return (self.is_auth > self.is_not_auth) and self.is_auth > 0.95

    def unauthentic(self):
        return (self.is_not_auth > self.is_auth) and self.is_not_auth > .95

    def correctness(self, users):
        if self.authentic() and not self.is_init:
            users_supporting    = filter(lambda c: c.result, self.checked.elems)
            users_opposing      = filter(lambda c: not c.result, self.checked.elems)

            users_supporting    = [users[c.index] for c in users_supporting]
            users_opposing      = [users[c.index] for c in users_opposing]

            valid_positive_checks = [user.authentic() for user in users_supporting]
            valid_negative_checks = [user.unauthentic() for user in users_opposing]

            valid_guesses = len(valid_positive_checks) + len(valid_negative_checks)
            all_guesses = len(users_supporting) + len(users_opposing)
            return valid_guesses/all_guesses
        elif self.is_init:
            return .95
        else:
            return 0

    def __str__(self):
        return f"<User: {self.id} {self.true_authenticity} {self.authentic()} {self.unauthentic()}>"

    def support(self, user):
        #if self.is_init:
        #    print(f"{self} supporters {user}")
        self.checked.push(Check_Result(user, True))

    def oppose(self, user):
        #if self.is_init:
        #    print(f"{self} opposes {user}")
        self.checked.push(Check_Result(user, False))

    def check_a_user(self, users):
        user_to_check = choose_user_to_check(self, [u for u in users if u != self])
        #print(f"{self} tries to check {user_to_check}")
        if user_to_check.true_authenticity:
            if random.random() < self.true_correctness:
                self.support(user_to_check)
            elif random.random() > self.true_correctness:
                self.oppose(user_to_check)
        else:
            if random.random() < self.true_correctness:
                self.oppose(user_to_check)
            elif random.random() > self.true_correctness:
                self.support(user_to_check)

def choose_user_to_check(user, users):
    unchecked_users = [u for u in users if u not in user.checked]
    if not user.is_init and len(unchecked_users) > 0:
        return random.choice(unchecked_users)
    elif not user.is_init and len(unchecked_users) == 0:
        return random.choice(users)
    else:
        if len(unchecked_users) == 0:
            unchecked_users = users
        return max(unchecked_users, key = lambda u: checks_performed_on(u, users))

def checks_performed_on(user, users):
    return len([u for u in users if user in u.checked])

class Check_Result(object):
    def __init__(self, user : User, result : bool):
        self.index = user.id
        self.result = result

    def __eq__(self, user : User):
        return user.id == self.index

def init(num_users):
    users = [User(i, is_init = i == 0) for i in range(num_users)]
    for i in range(len(users)):
        users[0].check_a_user(users)

    return users

def update(users, history = []):
    update_strategy = sys.argv[4] if len(sys.argv) >= 5 else ""
    if update_strategy == "spikes":
        new_users_count = 5 if random.random() < 0.8 else round(len(users)/4)
    elif update_strategy == "linear":
        new_users_count = 5
    elif update_strategy == "exponential":
        new_users_count = round(1.2 * len(users))
    else:
        new_users_count = 0

    new_users = [User(len(users) + i) for i in range(new_users_count)]
    users.extend(new_users)

    def check_randomly(times, users):
        for i in range(times):
            user = random.choice(users)
            user.check_a_user(users)

    checking_strategy = sys.argv[5] if len(sys.argv) >= 6 else ""
    if checking_strategy == "equal":
        check_randomly(new_users_count, users)
    elif checking_strategy == "delayed-equal":
        delay = int(sys.argv[6])
        if len(history) > delay:
            new_users_in_delay = len(history[-delay]) - len(history[-delay-1])
            print(f"delayed-equal checks: {new_users_in_delay}")
            check_randomly(new_users_in_delay, users)
    else:
        check_randomly(int(sys.argv[6]), users)

    for user in users:
        user.update(users)
    return users

def view(users, history):
    draw(history)

def draw(history):
    users_development       = [len(users) - len(history[i-1] if i != 0 else []) for users, i in zip(history, range(0, len(history)))]
    authentic_users_dev     = [len([u for u in users if u.true_authenticity])-len([u for u in history[i-1] if u.true_authenticity] if i != 0 else []) for users,i in zip(history, range(0, len(history)))]

    supposed_authentic_dev = [len([u for u in users if u.authentic()]) for users in history]
    supposed_unauthentic_dev = [len([u for u in users if u.unauthentic()]) for users in history]
    unknown_dev = [len([u for u in users if not u.authentic() and not u.unauthentic()]) for users in history]

    auths_in_recognized_auths = [len([u for u in users if u.authentic() and u.true_authenticity])/len([u for u in users if u.authentic()]) for users in history]
    unauths_in_recognized_auths = [len([u for u in users if u.authentic() and not u.true_authenticity])/len([u for u in users if u.authentic()]) for users in history]

    recognized_auths_per_auths = [len([u for u in users if u.authentic() and u.true_authenticity])/len([u for u in users if u.true_authenticity]) for users in history]

    fig, axes = plt.subplots(nrows = 3, ncols = 3, sharex= True)
    time    = [i for i in range(len(history))]

    axes[0,0].plot(time, users_development)
    axes[0,0].set_title("New users development")

    axes[0,1].plot(time, authentic_users_dev)
    axes[0,1].set_title("New authentic users development")

    axes[1,0].plot(time, supposed_authentic_dev)
    axes[1,0].set_title("Users supposed to be authentic")

    axes[1,1].plot(time, supposed_unauthentic_dev)
    axes[1,1].set_title("Users supposed to be unauthentic")

    axes[1,2].plot(time, unknown_dev)
    axes[1,2].set_title("Unclassified users")

    axes[2,0].plot(time,  auths_in_recognized_auths)
    axes[2,0].set_title("T. auths in c. auths")

    axes[2,1].plot(time, unauths_in_recognized_auths)
    axes[2,1].set_title("T. unauths in c. auths")

    axes[2,2].plot(time, recognized_auths_per_auths)
    axes[2,2].set_title("Recognized auths per total auths")

    plt.savefig(f"{len(history)}-{len(history[-1])}-{datetime.datetime.now().isoformat()}.png")
