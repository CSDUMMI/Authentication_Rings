"""
A Model, where is_auth and is_not_auth
are calculated as the average of all the
accuracies of the supporters and opponents.
"""
import os, random, datetime
from matplotlib import pyplot as plt

INIT_ACCURACY = float(os.environ["INIT_ACCURACY"]) if "INIT_ACCURACY" in os.environ else .95
ACTIVITY = int(os.environ["ACTIVITY"])
MULTIPLE = int(os.environ["MULTIPLE"]) if "MULTIPLE" in os.environ else False
ADDITION = int(os.environ["ADDITION"]) if "ADDITION" in os.environ else False
SPIKES = int(os.environ["SPIKES"]) if "SPIKES" in os.environ else False

INIT_SUPPORTERS = int(os.environ.get("INIT_SUPPORTERS", 20))
INIT_OPPONENTS = int(os.environ.get("INIT_OPPONENTS", 20))

class User():
    def __init__(self, id, is_init = False):
        self.id = id
        self.is_auth = 0 if not is_init else 1
        self.is_not_auth = 0
        self.supporters = []
        self.opponents = []
        self.true_authentic = random.uniform(0,1) < .5 if not is_init else True
        if is_init:
            self.true_accuracy = INIT_ACCURACY
        elif random.random() < 2/3:
            self.true_accuracy = random.uniform(0.7,1)
        else:
            self.true_accuracy = random.uniform(0,1)

        self.tested = set()
        self.test_count = 0
        self.is_init = is_init

    def authentic(self):
        return self.is_auth > self.is_not_auth

    def update(self, users):
        if not self.is_init:
            sum_corr = sum([u.accuracy(users) for u in self.supporters])
            self.is_auth = sum_corr/len(self.supporters) if len(self.supporters) > 0 else 0
            sum_corr = sum([u.accuracy(users) for u in self.opponents])
            self.is_not_auth = sum_corr/len(self.opponents) if len(self.supporters) > 0 else 0

    def accuracy(self, users):
        if is_init:
            return INIT_ACCURACY
        elif self.authentic():
            return 0

        supporting = [u for u in users if self in u.supporters]
        opposing = [u for u in users if self in u.opponents and self not in u.supporters]

        good_guesses = len([u for u in supporting if u.authentic()])
        good_guesses += len([u for u in opponents if not u.authentic()])

        all_guesses = len(supporting) + len(opposing)
        return good_guesses/all_guesses if all_guesses > 0 else 0

    def check(self, users):
        user = random.choice(users)

    def __check_user(self, user, users):
        if user.true_authentic and random.random() < self.true_accuracy and self not in user.supporters:
            user.supporters.append(self)
            if self in user.opponents:
                user.opponents.remove(self)
        elif not user.true_authentic and random.random() < self.true_accuracy and self not in user.opponents:
            user.opponents.append(self)
            if self in user.supporters:
                user.supporters.remove(self)
        elif user.true_authentic and random.random() > self.true_accuracy and self not in user.opponents:
            user.opponents.append(self)
            if self in user.supporters:
                user.supporters.remove(self)
        elif not user.true_authentic and random.random() > self.true_accuracy and self not in user.supporters:
            user.supporters.append(self)
            if self in user.opponents:
                user.opponents.remove(self)

    def test(self, user, users):
        for u in supporting_users(user, users):
            self.__check_user(u, users)

        for u in opposing_users(user, users):
            self.__check_user(u, users)

        self.tested.add(user)
        self.test_count += 1

def supporting_users(user,users):
    return [u for u in users if user in u.supporters]

def opposing_users(user, users):
    return [u for u in users if user in u.opponents]

def init(user_num):
    users = [User(i, is_init = i == 0) for i in range(user_num)]
    for user in users:
        user.supporters = random.sample([u for u in users if u != user], k = INIT_SUPPORTERS)
        user.opponents = random.sample([u for u in users if u != user and u not in user.supporters], k = INIT_OPPONENTS)
    return users

def update(users, history = []):
    if MULTIPLE != False:
        new_users *= len(users)
    elif ADDITION != False:
        new_users = ADDITION
    elif SPIKES != False:
        new_users = random.randint(0, len(users))
    else:
        new_users = 0

    users.extend([User(i + len(users)) for i in range(new_users)])

    test_me = set()
    for i in range(ACTIVITY):
        user = random.choice(users)
        user.check(users)
        if user.true_accuracy > .95:
            test_me.add(user)

    for user in users:
        user.update(users)

    init_user = users[0]
    for user in test_me:
        init_user.test(user, users)

    return users

def view(current_users, history):
    print(len([u for u in current_users if u.authentic()]))

    users_development       = [len(users) - len(history[i-1] if i != 0 else []) for users, i in zip(history, range(0, len(history)))]
    authentic_users_dev     = [len([u for u in users if u.true_authentic])-len([u for u in history[i-1] if u.true_authentic] if i != 0 else []) for users,i in zip(history, range(0, len(history)))]
    unauthentic_users_dev   = [len([u for u in users if not u.true_authentic])-len([u for u in history[i-1] if not u.true_authentic] if i != 0 else []) for users,i in zip(history, range(0, len(history)))]

    supposed_authentic_dev = [len([u for u in users if u.authentic()]) for users in history]
    supposed_unauthentic_dev = [len([u for u in users if not u.authentic()]) for users in history]

    auths_in_recognized_auths = [len([u for u in users if u.authentic() and u.true_authentic])/len([u for u in users if u.authentic()]) for users in history]
    unauths_in_recognized_auths = [len([u for u in users if u.authentic() and not u.true_authentic])/len([u for u in users if u.authentic()]) for users in history]

    recognized_auths_per_auths = [len([u for u in users if u.authentic() and u.true_authentic])/len([u for u in users if u.true_authentic]) for users in history]

    fig, axes = plt.subplots(nrows = 3, ncols = 3, sharex= True)
    time    = [i for i in range(len(history))]

    axes[0,0].plot(time, users_development)
    axes[0,0].set_title("New users")

    axes[0,1].plot(time, authentic_users_dev)
    axes[0,1].set_title("New authentic users")

    axes[0,2].plot(time, unauthentic_users_dev)
    axes[0,2].set_title("New unauhentic users")

    axes[1,0].plot(time, supposed_authentic_dev)
    axes[1,0].set_title("Users supposed to be authentic")

    axes[1,1].plot(time, supposed_unauthentic_dev)
    axes[1,1].set_title("Users supposed to be unauthentic")

    axes[2,0].plot(time,  auths_in_recognized_auths)
    axes[2,0].set_title("T. auths in c. auths")

    axes[2,1].plot(time, unauths_in_recognized_auths)
    axes[2,1].set_title("T. unauths in c. auths")

    axes[2,2].plot(time, recognized_auths_per_auths)
    axes[2,2].set_title("Recognized auths per total auths")

    plt.savefig(f"{len(history)}-{len(history[0])}-{datetime.datetime.now().isoformat()}.png")
    plt.show()
