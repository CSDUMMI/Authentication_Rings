import random, sys, math, threading, os
from matplotlib import pyplot as plt

class User(object):
    def __init__(self, id, is_init = False):
        self.id             = id
        self.is_auth        = 0 if not is_init else 1
        self.is_not_auth    = 0
        self.supporters     = set()
        self.opponents      = set()

        self._authentic         = random.random() < 0.5 if not is_init else True
        self.true_correctness   = random.random() if not is_init else .95

        self.is_init            = is_init
        self.init_strategy      = "most_activly_checked" if is_init else None

        self.is_evil    = random.random() < 0.7 if self.id > 1 else False
        if self.is_evil:
            self.target = self.id - random.randint(1, self.id-1)

        self.acted_maliciously  = False

    def _update_auth_scores(self, users):
        self.is_auth        = max([u.correctness(users) for u in self.supporters], default = 0) if not self.is_init else 1
        self.is_not_auth    = max([u.correctness(users) for u in self.opponents], default = 0) if not self.is_init else 0

    def authentic(self):
        return self.is_auth > self.is_not_auth

    def unauthentic(self):
        return self.is_auth < self.is_not_auth

    def correctness(self, users):
        if not self.is_init:
            if self.authentic():
                users_supporting    = [u for u in users if self in u.supporters]
                users_opposing      = [u for u in users if self in u.opponents]

                correct_supporting  = [u for u in users_supporting if u.authentic()]
                correct_opposing    = [u for u in users_opposing if u.unauthentic()]

                correct_guesses     = len(correct_supporting) + len(correct_opposing)
                all_guesses         = len(users_supporting) + len(users_opposing)
                if all_guesses != 0:
                    return correct_guesses/all_guesses
                else:
                    return 0
            else:
                return 0
        else:
            return 0.97

    def __str__(self):
        return f"<User: {self.id} {self._authentic} {self.true_correctness} {self.authentic()} {self.unauthentic()}>"

def choose_user_to_check(user,users):
    unchecked_users = [u for u in users if user not in u.supporters and user not in u.opponents]
    if user.is_init:
        if user.init_strategy == "most_activly_checked":
            user_to_check   = max(unchecked_users, key = lambda u: len(u.supporters) + len(u.opponents), default = random.choice(users))
        else:
            user_to_check   = max(unchecked_users, key = lambda u: u.is_auth - u.is_not_auth, default = max(users, key = lambda u: u.is_auth - u.is_not_auth))
    else:
        if len(unchecked_users) != 0:
            user_to_check   = random.choice(unchecked_users)
        else:
            user_to_check   = random.choice(users)

    return user_to_check

def update_user(user, users):
    checking_user   = choose_user_to_check(user, users)
    be_supporter    = False
    be_opponent     = False

    if checking_user._authentic:
        if random.random() < user.true_correctness:
            be_supporter    = True
        elif random.random() > user.true_correctness:
            be_opponent     = True
    else:
        if random.random() < user.true_correctness:
            be_opponent     = True
        elif random.random() > user.true_correctness:
            be_supporter    = True

    my_correctness = user.correctness(users)
    if be_supporter and my_correctness > checking_user.is_auth:
        checking_user.opponents.discard(user)
        checking_user.supporters.add(user)
    elif be_opponent and my_correctness > checking_user.is_not_auth:
        checking_user.supporters.discard(user)
        checking_user.opponents.add(user)

    if be_supporter and my_correctness > checking_user.is_auth:
        print(f"{str(user)} supports {str(checking_user)} with correctness: {user.correctness(users)}")
    elif be_opponent and my_correctness > checking_user.is_not_auth:
        print(f"{str(user)} opposes {str(checking_user)} with correctness: {user.correctness(users)}")
    checking_user._update_auth_scores(users)

    if user.is_evil and user.correctness(users) >= (max([u.correctness(users) for u in users[user.target].supporters], default = 0) if not users[user.target].is_init else 1):
        users[user.target].supporters.discard(user)
        users[user.target].opponents.add(user)
        user.acted_maliciously  = True

def update(users, init = False, round = 0, midpoint = 0, maximum_user_count = 0):
    for user in users:
        for i in range(int(len(users) / 4) if init and user.is_init else 1):
            update_user(user, users)

        if init and user.is_init:
            user.init_strategy  = "most_likely"

    if len(sys.argv) == 6 and sys.argv[4] == "linear":
        new_users = [User(users[-1].id + i) for i in range(1,int(sys.argv[5]))] # linear increase in users by steps of 5
        users.extend(new_users)
    elif len(sys.argv) == 5 and sys.argv[4] == "exponential":
        new_users = int(len(users) * 1.25)
        new_users = [User(users[-1].id + i) for i in range(1, new_users)]
        users.extend(new_users)
    elif len(sys.argv) == 5 and sys.argv[4] == "spikes":
        new_users = 5 if random.random() < .95 else int(len(users)/4)
        new_users = [User(users[-1].id + i) for i in range(1, new_users)]
        users.extend(new_users)
    elif len(sys.argv) >= 6 and sys.argv[4] == "sigmoid":
        new_users = int(random.gauss(int(sys.argv[3])/2, 0.2) * maximum_user_count)
        new_users = [User(users[-1].id + i) for i in range(1, new_users)]
        users.extend(new_users)

    return users

def init(users_count):
    users   = [User(i) for i in range(users_count -1)]
    users.append(User(users_count, is_init = True))
    users   = update(users, init = True)
    return users

def view(users, history):
    authentic_users         = [u for u in users if u._authentic]
    unauthentic_users       = [u for u in users if not u._authentic]
    recognized_auths        = [u for u in authentic_users if u.authentic()]
    recognized_unauths      = [u for u in unauthentic_users if u.unauthentic()]

    eligable_voters             = [u for u in users if u.authentic()]
    authentic_eligable_voters   = [u for u in eligable_voters if u._authentic]
    unauthentic_eligable_voters = [u for u in eligable_voters if not u._authentic]

    users_with_low_authenticity     = [u for u in eligable_voters if u.is_auth <= .9]
    users_with_low_unauthenticity   = [u for u in recognized_unauths if u.is_not_auth <= .9]

    evil_users              = [u for u in users if u.is_evil]
    acted_evil_users        = [u for u in evil_users if u.acted_maliciously]
    significant_evil_users  = [u for u in acted_evil_users if not users[u.target].authentic() and users[u.target]._authentic]

    users_as_str    = ""
    for user in users:
        users_as_str += f"""
User: {user.id}
Is authentic: {user._authentic}
True correctness: {user.true_correctness}
Is calculated authentic: {user.authentic()}
Calculated correctness: {user.correctness(users)}
Supporters: {[f'{u.id}:{u.correctness(users)}' for u in sorted(user.supporters, key = lambda s:s.correctness(users))]}
Opponents: {[f'{u.id}:{u.correctness(users)}' for u in sorted(user.opponents, key = lambda o:o.correctness(users))]}"""
    print (f"""
{users_as_str}

# of authentic users: {len(authentic_users)} or {len(authentic_users)/len(users) * 100}%
# of unauthentic users: {len(unauthentic_users)} or {len(unauthentic_users)/len(users) * 100}%

{(len(recognized_auths) + len(recognized_unauths)) / len(users) * 100}% of the users recognized as authentic/unauthentic.
{len(recognized_auths)/len(authentic_users) * 100}% of authentic users recognized.
{len(recognized_unauths)/len(unauthentic_users) * 100}% of unauthentic users recognized.
Eligable voters are:
{len(eligable_voters)/len(users) * 100}% of all users.
{len(authentic_eligable_voters)/len(authentic_users) * 100}% of all authentic users are eligable voters.
{len(authentic_eligable_voters)/len(eligable_voters) * 100}% of all eligable voters are authentic.
{len(unauthentic_eligable_voters)/len(eligable_voters) * 100}% of all eligable voters are unauthentic.

# of authentic users with lower than 20% authencitiy score: {len(users_with_low_authenticity)}
# of unauthentic users with lower than 20% unauthenticity score: {len(users_with_low_unauthenticity)}
# of evil users: {len(evil_users)}
# of evil users, that acted: {len([u for u in evil_users if u.acted_maliciously])}
# of significant evil users, that acted: {len(significant_evil_users)}
    """)
    draw(history)

def draw(history):
    users_development       = [len(users) - len(history[i-1] if i != 0 else []) for users, i in zip(history, range(0, len(history)))]
    authentic_users_dev     = [len([u for u in users if u._authentic]) for users in history]
    unauthentic_users_dev   = [len([u for u in users if not u._authentic]) for users in history]

    assumed_auths_dev   = [(len([u for u in users if u.authentic()]) - len([u for u in history[i-1] if u.authentic()] if i != 0 else [])) for users, i in zip(history, range(0, len(history)))]

    auths_among_eligable_voters     = [len([u for u in users if u.authentic() and u._authentic])/len([u for u in users if u.authentic()]) for users in history]
    unauths_amog_eligable_voters    = [len([u for u in users if u.authentic() and not u._authentic])/len([u for u in users if u.authentic()]) for users in history]

    eligable_authentic_voters_per_authentic_users   = [len([u for u in users if u.authentic() and u._authentic])/len([u for u in users if u._authentic]) for users in history]

    fig, axes = plt.subplots(nrows = 3, ncols = 2, sharex= True)
    time    = [i for i in range(len(history))]
    axes[0, 0].plot(time, users_development)
    axes[0, 0].set_title("New users")

    axes[0, 1].plot(time, assumed_auths_dev)
    axes[0, 1].set_title("New calculated authentic users")

    axes[1, 0].plot(time, auths_among_eligable_voters)
    axes[1, 0].set_title("authentic users/eligable voters")

    axes[1, 1].plot(time, unauths_amog_eligable_voters)
    axes[1, 1].set_title("unauthentic users/eligable voters")

    axes[2, 0].plot(time, eligable_authentic_voters_per_authentic_users)
    axes[2, 0].set_title("eligable authentic voters per authentic users")

    axes[2, 1].plot(time, unauthentic_users_dev)
    axes[2, 1].set_title("unauthentic users development")

    plt.savefig(f"{len(history)}-{len(history[-1])}-{datetime.datetime.now().isoformat()}.png")
    plt.show()
