"""
Using the Test Cycle to optimize the Avg Model.
It works like this:
1. A user checks a few other users with an accuracy of 0.
2. The user applies for a test by the init user.
3. The init user checks all the users by the user, then giving the user an approriate accuracy.
=> After the init user or another user tested by init has tested you, you get an appropriate accuracy

So, the modelling will go like this:
num_users will be created.
The first user will be the init user.

Every user will start by checking randomly each other for first_phase_length.
Then every user with a high self esteem (calculated - true accuracy)
will apply for testing and the init user will go through them,
testing and verifying their checks.

1. Create num_users users.
2. Randomly check each other. (Don't take them seriously, until they are tested)
3. Let the users, where calculated - true accuracy > high_esteem, apply for tests by init user or another user with accuracy > 0.
4. Give accuracy values
5. Repeat at 2.
6. Maybe reward users with high testing scores. Gamifing the whole process.
"""
first_phase_length = int(os.environ.get("first_phase_length", 10))
high_esteem = int(os.environ.get("hgih_esteem", 10))

class User(object):

    def __init__(self, id, is_init = False):
        super(User, self).__init__()
        self.id = id
        self.is_init = is_init

def init(num_users):
    users = [User(i, is_init = (i == 0)) for i in range(num_users)]
    return users

def update(users):
    if len(history) %  > 0:
        user = random.choice(users)
        user.check(users)
    elif len(history) % os.environ.get("first_phase_length", 10) == 0:
        users_to_test = [u for u in users if u.accuracy(users) - u.true_accuracy > high_esteem]
        for user_to_test in users_to_test:
            testing_user = max([u for u in users if u.accuracy(users) > 0], key = lambda u: u.accuracy())
            testing_user.test(user_to_test)

def view(current_users, history):
    print(len([u for u in current_users if u.authentic()]))

    users_development       = [len(users) - len(history[i-1] if i != 0 else []) for users, i in zip(history, range(0, len(history)))]
    authentic_users_dev     = [len([u for u in users if u.true_authentic])-len([u for u in history[i-1] if u.true_authentic] if i != 0 else []) for users,i in zip(history, range(0, len(history)))]
    unauthentic_users_dev   = [len([u for u in users if not u.true_authentic])-len([u for u in history[i-1] if not u.true_authentic] if i != 0 else []) for users,i in zip(history, range(0, len(history)))]

    supposed_authentic_dev = [len([u for u in users if u.authentic()]) for users in history]
    supposed_unauthentic_dev = [len([u for u in users if not u.authentic()]) for users in history]

    auths_in_recognized_auths = [len([u for u in users if u.authentic() and u.true_authentic])/len([u for u in users if u.authentic()]) for users in history]
    unauths_in_recognized_auths = [len([u for u in users if u.authentic() and not u.true_authentic])/len([u for u in users if u.authentic()]) for users in history]

    recognized_auths_per_auths = [len([u for u in users if u.authentic() and u.true_authentic])/len([u for u in users if u.true_authentic]) for users in history]

    fig, axes = plt.subplots(nrows = 3, ncols = 3, sharex= True)
    time    = [i for i in range(len(history))]

    axes[0,0].plot(time, users_development)
    axes[0,0].set_title("New users")

    axes[0,1].plot(time, authentic_users_dev)
    axes[0,1].set_title("New authentic users")

    axes[0,2].plot(time, unauthentic_users_dev)
    axes[0,2].set_title("New unauhentic users")

    axes[1,0].plot(time, supposed_authentic_dev)
    axes[1,0].set_title("Users supposed to be authentic")

    axes[1,1].plot(time, supposed_unauthentic_dev)
    axes[1,1].set_title("Users supposed to be unauthentic")

    axes[2,0].plot(time,  auths_in_recognized_auths)
    axes[2,0].set_title("T. auths in c. auths")

    axes[2,1].plot(time, unauths_in_recognized_auths)
    axes[2,1].set_title("T. unauths in c. auths")

    axes[2,2].plot(time, recognized_auths_per_auths)
    axes[2,2].set_title("Recognized auths per total auths")

    plt.savefig(f"{len(history)}-{len(history[0])}-{datetime.datetime.now().isoformat()}.png")
    plt.show()
