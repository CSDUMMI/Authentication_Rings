import random, sys, math
from pprint import pprint

class User(object):
    def __init__(self, id):
        self.id                      = id
        self.authentication_score    = 0
        self.correctness_score       = 0
        self.true_correctness_score  = random.uniform(0,1)
        self.authentic               = random.uniform(0,1) < 0.5
        self.supporters              = set()
        self.is_init_user            = False

def update(users):
    for user in users:
        random_user = random.sample({ u for u in users if u != user }, 1)[0]

        if random_user.authentic:
            add_support = random.uniform(0,1) < user.true_correctness_score
        else:
            add_support = random.uniform(0,1) > user.true_correctness_score

        if add_support:
            random_user.supporters.add(user)
            print(f"#{user.id} supports #{random_user.id} with a correctness of {user.correctness_score * 100}%")

    for user in users:
        users_supporting            = { u for u in users if (user in u.supporters) }

        if len(users_supporting) >= math.sqrt(len(users)) or len(users_supporting) >= 250:
            user.correctness_score      = len({ u for u in users_supporting if u.authentication_score >= 0.95 }) / len(users_supporting) if len(users_supporting) > 0 and not user.is_init_user else user.correctness_score

        user.authentication_score   = max([ support.correctness_score for support in user.supporters ], default = user.authentication_score) if not user.is_init_user else user.authentication_score

        if user.correctness_score == 1:
            print(f"#{user.id} has a correctness score of { user.correctness_score }%")
            pprint({ u.id for u in users_supporting })


    return users

def print_users(users):
    for user in users:
        print(f"""====================
ID: {user.id}
Authentication Score: {user.authentication_score * 100}%
Correctness Score: {user.correctness_score * 100}%
Real correctness Score: {user.true_correctness_score * 100}%
Authentic: {user.authentic}
Is init user: {user.is_init_user}
        """)

def init():
    users           = { User(i) for i in range(int(sys.argv[1])) }
    initial_user    = User(int(sys.argv[1]) + 1)
    initial_user.authentication_score   = 1
    initial_user.correctness_score      = 0.95
    initial_user.true_correctness_score = 0.95
    initial_user.authentic              = True
    initial_user.is_init_user           = True
    users.add(initial_user)

    print("Initial users")
    print_users(users)
    return users

if __name__ == "__main__":
    users = init()

    for i in range(int(sys.argv[2])):
        users = update(users)

    authentic_users     = { user for user in users if user.authentic }
    unauthentic_users   = { user for user in users if not user.authentic }
    recongnized_authentic_users = { user for user in authentic_users if user.authentication_score >= 0.95 }
    unrecongnized_unauthentic_users   = { user for user in unauthentic_users if user.authentication_score >= 0.95 }

    print(f"After updationg for {sys.argv[2]} times")
    print_users(users)
    print(f"""# of authentic users: {len(authentic_users)}
# of unauthentic users: {len(unauthentic_users)}
Rate of recognized authentic users: {len(recongnized_authentic_users)/len(authentic_users) * 100}%
Rate of users, that approached their true correctness: {len({u for u in users if math.isclose(u.true_correctness_score, u.correctness_score, abs_tol = .01) })/len(users)}
Rate of unrecognized unauthentic users: {len(unrecongnized_unauthentic_users)/len(unauthentic_users) * 100}%
    """)
